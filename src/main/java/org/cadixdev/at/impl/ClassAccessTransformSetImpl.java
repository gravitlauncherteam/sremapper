/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.at.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

import org.cadixdev.at.AccessTransform;
import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.bombe.analysis.InheritanceProvider;
import org.cadixdev.bombe.type.signature.MethodSignature;

class ClassAccessTransformSetImpl implements AccessTransformSet.Class {

	private AccessTransform allFields = AccessTransform.EMPTY;
	private AccessTransform allMethods = AccessTransform.EMPTY;

	private AccessTransform classTransform = AccessTransform.EMPTY;
	private boolean complete;
	private final Map<String, AccessTransform> fields = new HashMap<>();

	private final Map<MethodSignature, AccessTransform> methods = new HashMap<>();
	private final String name;

	private final AccessTransformSet parent;

	ClassAccessTransformSetImpl(final AccessTransformSet parent, final String name) {
		this.parent = parent;
		this.name = name;
	}

	@Override
	public AccessTransform allFields() {
		return allFields;
	}

	@Override
	public AccessTransform allMethods() {
		return allMethods;
	}

	@Override
	public void complete(final InheritanceProvider provider, final InheritanceProvider.ClassInfo info) {
		if (complete)
			return;

		for (final InheritanceProvider.ClassInfo parent : info.provideParents(provider)) {
			final AccessTransformSet.Class parentAts = getParent().getOrCreateClass(parent.getName());
			parentAts.complete(provider, parent);

			parentAts.getMethods().forEach((signature, transform) -> {
				if (info.overrides(signature, parent))
					mergeMethod(signature, transform);
			});
		}

		complete = true;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ClassAccessTransformSetImpl))
			return false;

		final ClassAccessTransformSetImpl that = (ClassAccessTransformSetImpl) o;
		return classTransform.equals(that.classTransform) && allFields.equals(that.allFields)
				&& allMethods.equals(that.allMethods) && fields.equals(that.fields) && methods.equals(that.methods);
	}

	@Override
	public AccessTransform get() {
		return classTransform;
	}

	@Override
	public AccessTransform getField(final String name) {
		return fields.getOrDefault(Objects.requireNonNull(name, "name"), allFields);
	}

	@Override
	public Map<String, AccessTransform> getFields() {
		return Collections.unmodifiableMap(fields);
	}

	@Override
	public AccessTransform getMethod(final MethodSignature signature) {
		return methods.getOrDefault(Objects.requireNonNull(signature, "signature"), allMethods);
	}

	@Override
	public Map<MethodSignature, AccessTransform> getMethods() {
		return Collections.unmodifiableMap(methods);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AccessTransformSet getParent() {
		return parent;
	}

	@Override
	public int hashCode() {
		return Objects.hash(classTransform, allFields, allMethods, fields, methods);
	}

	@Override
	public boolean isComplete() {
		return complete;
	}

	@Override
	public AccessTransform merge(final AccessTransform transform) {
		return classTransform = classTransform.merge(transform);
	}

	@Override
	public void merge(final AccessTransformSet.Class other) {
		Objects.requireNonNull(other, "other");

		merge(other.get());
		mergeAllFields(other.allFields());
		mergeAllMethods(other.allMethods());

		other.getFields().forEach(this::mergeField);
		other.getMethods().forEach(this::mergeMethod);
	}

	@Override
	public AccessTransform mergeAllFields(final AccessTransform transform) {
		return allFields = allFields.merge(transform);
	}

	@Override
	public AccessTransform mergeAllMethods(final AccessTransform transform) {
		return allMethods = allMethods.merge(transform);
	}

	@Override
	public AccessTransform mergeField(final String name, final AccessTransform transform) {
		Objects.requireNonNull(name, "name");
		Objects.requireNonNull(transform, "transform");

		if (transform.isEmpty())
			return fields.getOrDefault(name, AccessTransform.EMPTY);
		return fields.merge(name, transform, AccessTransform::merge);
	}

	@Override
	public AccessTransform mergeMethod(final MethodSignature signature, final AccessTransform transform) {
		Objects.requireNonNull(signature, "signature");
		Objects.requireNonNull(transform, "transform");

		if (transform.isEmpty())
			return methods.getOrDefault(signature, AccessTransform.EMPTY);
		return methods.merge(signature, transform, AccessTransform::merge);
	}

	@Override
	public AccessTransform replace(final AccessTransform transform) {
		return classTransform = Objects.requireNonNull(transform, "transform");
	}

	@Override
	public AccessTransform replaceAllFields(final AccessTransform transform) {
		return allFields = Objects.requireNonNull(transform, "transform");
	}

	@Override
	public AccessTransform replaceAllMethods(final AccessTransform transform) {
		return allMethods = Objects.requireNonNull(transform, "transform");
	}

	@Override
	public AccessTransform replaceField(final String name, final AccessTransform transform) {
		Objects.requireNonNull(name, "name");
		Objects.requireNonNull(transform, "transform");

		if (transform.isEmpty())
			return fields.remove(name);
		return fields.put(name, transform);
	}

	@Override
	public AccessTransform replaceMethod(final MethodSignature signature, final AccessTransform transform) {
		Objects.requireNonNull(signature, "signature");
		Objects.requireNonNull(transform, "transform");

		if (transform.isEmpty())
			return methods.remove(signature);
		return methods.put(signature, transform);
	}

	@Override
	public String toString() {
		final StringJoiner joiner = new StringJoiner(", ", "AccessTransformSet.Class{", "}");
		if (!classTransform.isEmpty())
			joiner.add(classTransform.toString());
		if (!allFields.isEmpty())
			joiner.add("allFields=" + allFields);
		if (!allMethods.isEmpty())
			joiner.add("allMethods=" + allMethods);
		if (!fields.isEmpty())
			joiner.add("fields=" + fields);
		if (!methods.isEmpty())
			joiner.add("method=" + methods);
		return joiner.toString();
	}
}
