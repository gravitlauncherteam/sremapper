/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.at;

import java.util.Map;
import java.util.Optional;

import org.cadixdev.at.impl.AccessTransformSetImpl;
import org.cadixdev.bombe.analysis.InheritanceCompletable;
import org.cadixdev.bombe.analysis.InheritanceProvider;
import org.cadixdev.bombe.type.signature.MethodSignature;
import org.cadixdev.lorenz.MappingSet;

public interface AccessTransformSet {

	interface Class extends InheritanceCompletable {
		AccessTransform allFields();

		AccessTransform allMethods();

		AccessTransform get();

		AccessTransform getField(String name);

		Map<String, AccessTransform> getFields();

		AccessTransform getMethod(MethodSignature signature);

		Map<MethodSignature, AccessTransform> getMethods();

		String getName();

		AccessTransformSet getParent();

		AccessTransform merge(AccessTransform transform);

		void merge(Class other);

		AccessTransform mergeAllFields(AccessTransform transform);

		AccessTransform mergeAllMethods(AccessTransform transform);

		AccessTransform mergeField(String name, AccessTransform transform);

		AccessTransform mergeMethod(MethodSignature signature, AccessTransform transform);

		@Override
		default Optional<InheritanceProvider.ClassInfo> provideInheritance(final InheritanceProvider provider,
				final Object context) {
			return provider.provide(getName(), context);
		}

		AccessTransform replace(AccessTransform transform);

		AccessTransform replaceAllFields(AccessTransform transform);

		AccessTransform replaceAllMethods(AccessTransform transform);

		AccessTransform replaceField(String name, AccessTransform transform);

		AccessTransform replaceMethod(MethodSignature signature, AccessTransform transform);

	}

	static AccessTransformSet create() {
		return new AccessTransformSetImpl();
	}

	Optional<Class> getClass(String name);

	Map<String, Class> getClasses();

	Class getOrCreateClass(String name);

	void merge(AccessTransformSet other);

	AccessTransformSet remap(MappingSet mappings);

	Optional<Class> removeClass(String name);

}
