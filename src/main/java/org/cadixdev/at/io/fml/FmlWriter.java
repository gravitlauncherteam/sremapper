/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.at.io.fml;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.function.BiConsumer;

import org.cadixdev.at.AccessChange;
import org.cadixdev.at.AccessTransform;
import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.bombe.type.signature.MethodSignature;

final class FmlWriter {

	private interface ThrowingBiConsumer<T, U> extends BiConsumer<T, U> {

		@SuppressWarnings("unchecked")
		static <E extends Exception> void rethrow(final Exception e) throws E {
			throw (E) e;
		}

		@Override
		default void accept(final T t, final U u) {
			try {
				throwingAccept(t, u);
			} catch (final IOException e) {
				rethrow(e);
			}
		}

		void throwingAccept(T t, U u) throws IOException;

	}

	private static final String METHOD_WILDCARD = "*()";

	private static final char WILDCARD = '*';

	private static String getAccessModifier(final AccessChange change) {
		switch (change) {
		case PUBLIC:
			return "public";
		case PROTECTED:
			return "protected";
		case PACKAGE_PRIVATE:
			return "default";
		case PRIVATE:
			return "private";
		default:
			throw new AssertionError(change);
		}
	}

	private static <T, U> BiConsumer<T, U> throwing(final ThrowingBiConsumer<T, U> consumer) {
		return consumer;
	}

	private final BufferedWriter writer;

	FmlWriter(final BufferedWriter writer) {
		this.writer = writer;
	}

	void write(final AccessTransformSet set) throws IOException {
		set.getClasses().forEach(throwing((originalClassName, classSet) -> {
			final String className = originalClassName.replace('/', '.');

			writeClass(className, classSet.get());

			writeField(className, null, classSet.allFields());
			classSet.getFields().forEach(throwing((name, transform) -> writeField(className, name, transform)));

			writeMethod(className, null, classSet.allMethods());
			classSet.getMethods().forEach(throwing((name, transform) -> writeMethod(className, name, transform)));
		}));
	}

	private void writeAccessTransform(final String className, final AccessTransform transform) throws IOException {
		if (transform.getAccess() != AccessChange.NONE)
			writer.write(getAccessModifier(transform.getAccess()));

		switch (transform.getFinal()) {
		case NONE:
			break;
		case REMOVE:
			writer.write("-f");
			break;
		case ADD:
			writer.write("+f");
			break;
		default:
			throw new AssertionError(transform.getFinal());
		}

		writer.write(' ');
		writer.write(className);
	}

	private void writeClass(final String className, final AccessTransform transform) throws IOException {
		if (transform.isEmpty())
			return;

		writeAccessTransform(className, transform);
		writer.newLine();
	}

	private void writeField(final String className, final String name, final AccessTransform transform)
			throws IOException {
		if (transform.isEmpty())
			return;

		writeAccessTransform(className, transform);
		writer.write(' ');

		if (name != null)
			writer.write(name);
		else
			writer.write(WILDCARD);
		writer.newLine();
	}

	private void writeMethod(final String className, final MethodSignature signature, final AccessTransform transform)
			throws IOException {
		if (transform.isEmpty())
			return;

		writeAccessTransform(className, transform);
		writer.write(' ');

		if (signature != null) {
			writer.write(signature.getName());
			writer.write(signature.getDescriptor().toString());
		} else
			writer.write(METHOD_WILDCARD);
		writer.newLine();
	}

}
