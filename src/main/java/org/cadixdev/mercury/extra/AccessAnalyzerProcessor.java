/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury.extra;

import static org.cadixdev.mercury.util.BombeBindings.convertSignature;
import static org.cadixdev.mercury.util.BombeBindings.isPackagePrivate;

import java.util.Objects;

import org.cadixdev.at.AccessChange;
import org.cadixdev.at.AccessTransform;
import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.at.ModifierChange;
import org.cadixdev.bombe.analysis.InheritanceProvider;
import org.cadixdev.bombe.type.signature.MethodSignature;
import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.model.ClassMapping;
import org.cadixdev.mercury.SourceContext;
import org.cadixdev.mercury.SourceProcessor;
import org.cadixdev.mercury.analysis.MercuryInheritanceProvider;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SimpleName;

/**
 * Generates access transformers for fields/method that would no longer be
 * accessible when moving classes to other packages.
 */
public final class AccessAnalyzerProcessor implements SourceProcessor {

	private static class Visitor extends ASTVisitor {

		private static final AccessTransform TRANSFORM = AccessTransform.of(AccessChange.PUBLIC, ModifierChange.NONE);

		private static boolean inheritsBinding(ASTNode node, final ITypeBinding declaringClass) {
			while (node != null) {
				final ITypeBinding parentBinding = resolveBinding(node);
				if (parentBinding != null && parentBinding.isAssignmentCompatible(declaringClass))
					return true;

				node = node.getParent();
			}

			return false;
		}

		private static ITypeBinding resolveBinding(final ASTNode node) {
			if (node instanceof AbstractTypeDeclaration)
				return ((AbstractTypeDeclaration) node).resolveBinding();

			if (node.getNodeType() == ASTNode.ANONYMOUS_CLASS_DECLARATION)
				return ((AnonymousClassDeclaration) node).resolveBinding();

			return null;
		}

		private final AccessTransformSet ats;
		private final InheritanceProvider inheritanceProvider;

		private final MappingSet mappings;

		private final String newPackage;

		private Visitor(final SourceContext context, final AccessTransformSet ats, final MappingSet mappings) {
			this.ats = ats;
			this.mappings = mappings;
			inheritanceProvider = MercuryInheritanceProvider.get(context.getMercury());

			newPackage = this.mappings.getTopLevelClassMapping(context.getQualifiedPrimaryType())
					.map(primary -> primary.getDeobfuscatedPackage().replace('/', '.'))
					.orElse(context.getPackageName());
		}

		private void analyze(final SimpleName node, final IMethodBinding binding) {
			final ITypeBinding declaringClass = binding.getDeclaringClass();
			if (needsTransform(node, binding, declaringClass)) {
				final MethodSignature signature = convertSignature(binding);
				ats.getOrCreateClass(declaringClass.getBinaryName()).mergeMethod(signature, TRANSFORM);
			}
		}

		private void analyze(final SimpleName node, final ITypeBinding binding) {
			if (needsTransform(node, binding, binding))
				ats.getOrCreateClass(binding.getBinaryName()).merge(TRANSFORM);
		}

		private void analyze(final SimpleName node, final IVariableBinding binding) {
			if (!binding.isField())
				return;

			final ITypeBinding declaringClass = binding.getDeclaringClass();
			if (needsTransform(node, binding, declaringClass))
				ats.getOrCreateClass(declaringClass.getBinaryName()).mergeField(binding.getName(), TRANSFORM);
		}

		private boolean needsTransform(final SimpleName node, final IBinding binding,
				final ITypeBinding declaringClass) {
			if (declaringClass == null || declaringClass.getBinaryName() == null)
				return false;

			final int modifiers = binding.getModifiers();
			if (Modifier.isProtected(modifiers)) {
				if (inheritsBinding(node, declaringClass))
					return false;
			} else if (!isPackagePrivate(modifiers))
				return false;

			final ClassMapping<?, ?> mapping = mappings.getClassMapping(declaringClass.getBinaryName()).orElse(null);

			String packageName;
			if (mapping != null) {
				mapping.complete(inheritanceProvider, declaringClass);
				packageName = mapping.getDeobfuscatedPackage().replace('/', '.');
			} else
				packageName = declaringClass.getPackage().getName();

			return !packageName.equals(newPackage);
		}

		@Override
		public boolean visit(final SimpleName node) {
			final IBinding binding = node.resolveBinding();
			if (binding == null)
				return true;

			switch (binding.getKind()) {
			case IBinding.TYPE:
				analyze(node, ((ITypeBinding) binding).getErasure());
				break;
			case IBinding.METHOD:
				analyze(node, ((IMethodBinding) binding).getMethodDeclaration());
				break;
			case IBinding.VARIABLE:
				analyze(node, ((IVariableBinding) binding).getVariableDeclaration());
				break;
			}

			return true;
		}

	}

	public static AccessAnalyzerProcessor create(final AccessTransformSet ats, final MappingSet mappings) {
		return new AccessAnalyzerProcessor(ats, mappings);
	}

	private final AccessTransformSet ats;

	private final MappingSet mappings;

	private AccessAnalyzerProcessor(final AccessTransformSet ats, final MappingSet mappings) {
		this.ats = Objects.requireNonNull(ats, "ats");
		this.mappings = Objects.requireNonNull(mappings, "mappings");
	}

	@Override
	public int getFlags() {
		return FLAG_RESOLVE_BINDINGS;
	}

	@Override
	public void process(final SourceContext context) {
		context.getCompilationUnit().accept(new Visitor(context, ats, mappings));
	}

}
