/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury.extra;

import java.util.List;
import java.util.Optional;

import org.cadixdev.mercury.RewriteContext;
import org.cadixdev.mercury.SourceRewriter;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;

/**
 * Removes redundant synthetic bridge methods that cause compile errors.
 */
public final class BridgeMethodRewriter implements SourceRewriter {

	private static class Visitor extends ASTVisitor {

		private static IMethodBinding findBridgedMethod(final MethodDeclaration node) {
			final Block body = node.getBody();
			if (body == null)
				return null;

			@SuppressWarnings("unchecked")
			final List<Statement> statements = body.statements();
			if (statements.size() != 1)
				return null;

			final Statement statement = statements.get(0);
			Expression expression;

			switch (statement.getNodeType()) {
			case ASTNode.EXPRESSION_STATEMENT:
				expression = ((ExpressionStatement) statement).getExpression();
				break;
			case ASTNode.RETURN_STATEMENT:
				expression = ((ReturnStatement) statement).getExpression();
				break;
			default:
				return null;
			}

			if (expression == null || expression.getNodeType() != ASTNode.METHOD_INVOCATION)
				return null;

			final MethodInvocation invocation = (MethodInvocation) expression;
			expression = invocation.getExpression();
			if (expression == null || expression.getNodeType() != ASTNode.THIS_EXPRESSION)
				return null;

			if (((ThisExpression) expression).getQualifier() != null)
				return null;

			@SuppressWarnings("unchecked")
			final List<Expression> arguments = invocation.arguments();
			if (arguments.size() != node.parameters().size())
				return null;

			for (final Expression arg : arguments)
				switch (arg.getNodeType()) {
				case ASTNode.SIMPLE_NAME:
					continue;
				case ASTNode.CAST_EXPRESSION:
					if (((CastExpression) arg).getExpression().getNodeType() == ASTNode.SIMPLE_NAME)
						continue;
				default:
					return null;
				}

			return invocation.resolveMethodBinding().getMethodDeclaration();
		}

		private static String getIdentifier(final SimpleName name, final Optional<ASTRewrite> rewrite) {
			return rewrite.map(r -> (String) r.get(name, SimpleName.IDENTIFIER_PROPERTY))
					.orElseGet(name::getIdentifier);
		}

		private final RewriteContext context;

		private Visitor(final RewriteContext context) {
			this.context = context;
		}

		@Override
		public boolean visit(final MethodDeclaration node) {
			final IMethodBinding bridged = findBridgedMethod(node);
			if (bridged == null)
				return true;

			final MethodDeclaration other = (MethodDeclaration) context.getCompilationUnit().findDeclaringNode(bridged);
			if (other == null)
				return true;

			final Optional<ASTRewrite> rewrite = context.getASTRewrite();
			final String name = getIdentifier(node.getName(), rewrite);
			final String otherName = getIdentifier(other.getName(), rewrite);
			if (!name.equals(otherName))
				return true;

			// Check if the two methods would clash (due to same parameter types)
			final IMethodBinding binding = node.resolveBinding();
			if (binding == null) return true;
			final ITypeBinding[] myTypes = binding.getParameterTypes();
			final ITypeBinding[] otherTypes = bridged.getParameterTypes();
			if (myTypes.length != otherTypes.length)
				return true;

			for (int i = 0; i < myTypes.length; i++)
				if (!myTypes[i].getErasure().isEqualTo(otherTypes[i].getErasure()))
					return true;

			// Remove the bridge method
			context.createASTRewrite().remove(node, null);

			return true;
		}

	}

	private static final SourceRewriter INSTANCE = new BridgeMethodRewriter();

	public static SourceRewriter create() {
		return INSTANCE;
	}

	private BridgeMethodRewriter() {
	}

	@Override
	public int getFlags() {
		return FLAG_RESOLVE_BINDINGS;
	}

	@Override
	public void rewrite(final RewriteContext context) {
		context.getCompilationUnit().accept(new Visitor(context));
	}

}
