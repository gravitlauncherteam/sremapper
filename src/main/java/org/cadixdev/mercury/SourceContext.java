/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jface.text.Document;

public class SourceContext {

	private final CompilationUnit compilationUnit;

	private Document document;
	private final Mercury mercury;

	String packageName;
	String primaryType;

	private final Path sourceFile;

	SourceContext(final Mercury mercury, final Path sourceFile, final CompilationUnit compilationUnit,
			final String primaryType) {
		this.mercury = mercury;
		this.sourceFile = sourceFile;
		this.compilationUnit = compilationUnit;

		final PackageDeclaration packageDeclaration = compilationUnit.getPackage();
		packageName = packageDeclaration != null ? packageDeclaration.getName().getFullyQualifiedName() : "";
		this.primaryType = primaryType;
	}

	public final CompilationUnit getCompilationUnit() {
		return compilationUnit;
	}

	public final Mercury getMercury() {
		return mercury;
	}

	public final String getPackageName() {
		return packageName;
	}

	public final String getPrimaryType() {
		return primaryType;
	}

	public final String getQualifiedPrimaryType() {
		if (packageName.isEmpty())
			return primaryType;
		else
			return packageName + '.' + primaryType;
	}

	public final Path getSourceFile() {
		return sourceFile;
	}

	public final Document loadDocument() throws IOException {
		if (document == null)
			document = new Document(new String(Files.readAllBytes(sourceFile), StandardCharsets.UTF_8));
		return document;
	}

	void process(final List<SourceProcessor> processors) throws Exception {
		for (final SourceProcessor processor : processors)
			processor.process(this);
	}

}
