/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FileASTRequestor;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;

public final class Mercury {

	private class Requestor extends FileASTRequestor {

		@Override
		public void acceptAST(final String sourceFilePath, final CompilationUnit ast) {
			accept(sourceFilePath, ast);
		}

	}

	private static final String[] EMPTY_STRING_ARRAY = new String[0];

	public static final String JAVA_EXTENSION = ".java";

	private static boolean isAnonymousOrLocalType(final String className) {
		int i = className.indexOf('$') + 1;
		while (i > 0 && i < className.length()) {
			if (Character.isDigit(className.charAt(i)))
				return true;
			i = className.indexOf('$', i) + 1;
		}
		return false;
	}

	private static String[] toArray(final Stream<Path> stream) {
		return stream.map(Path::toString).toArray(String[]::new);
	}

	private final List<Path> classPath = new ArrayList<>();

	private final Map<Object, Object> context = new HashMap<>();
	private Charset encoding = StandardCharsets.UTF_8;
	private Path outputDir;

	private final List<SourceProcessor> processors = new ArrayList<>();

	private final FileASTRequestor requestor = new Requestor();

	private String sourceCompatibility = JavaCore.VERSION_1_8;

	private Path sourceDir;

	private final List<Path> sourcePath = new ArrayList<>();

	void accept(final String sourceFilePath, final CompilationUnit ast) {
		final SourceContext context = createContext(sourceFilePath, ast);

		try {
			context.process(processors);
		} catch (final Exception e) {
			throw new RuntimeException("Failed to process: " + sourceFilePath, e);
		}
	}

	private void cleanup() {
		sourceDir = null;
		outputDir = null;
		context.clear();
	}

	private SourceContext createContext(final String sourceFilePath, final CompilationUnit ast) {
		final Path sourceFile = Paths.get(sourceFilePath);
		final String fileName = sourceFile.getFileName().toString();
		final String primaryType = fileName.substring(0, fileName.length() - JAVA_EXTENSION.length());

		if (outputDir != null)
			return new RewriteContext(this, sourceFile, ast, primaryType);
		else
			return new SourceContext(this, sourceFile, ast, primaryType);
	}

	public Optional<ITypeBinding> createTypeBinding(final String className) {
		if (isAnonymousOrLocalType(className))
			// TODO: Anonymous or local types are currently not supported
			// Eclipse uses source lines in their binding keys that are impossible
			// to know in advance. Since it may return incorrect results, abort early.
			return Optional.empty();

		final IBinding binding = requestor.createBindings(new String[] { 'L' + className.replace('.', '/') + ';' })[0];
		return binding != null && binding.getKind() == IBinding.TYPE ? Optional.of((ITypeBinding) binding)
				: Optional.empty();
	}

	public List<Path> getClassPath() {
		return classPath;
	}

	public Map<Object, Object> getContext() {
		return context;
	}

	public Charset getEncoding() {
		return encoding;
	}

	// Assume that all files use the same encoding
	private String[] getEncodings(final String[] files) {
		if (files.length == 0)
			return EMPTY_STRING_ARRAY;

		final String[] encodings = new String[files.length];
		Arrays.fill(encodings, encoding.name());
		return encodings;
	}

	public Path getOutputDir() {
		return outputDir;
	}

	public List<SourceProcessor> getProcessors() {
		return processors;
	}

	public String getSourceCompatibility() {
		return sourceCompatibility;
	}

	public Path getSourceDir() {
		return sourceDir;
	}

	public List<Path> getSourcePath() {
		return sourcePath;
	}

	public void process(final Path sourceDir) throws Exception {
		if (this.sourceDir != null)
			throw new IllegalStateException("Instance is currently processing: " + this.sourceDir);

		try {
			this.sourceDir = Objects.requireNonNull(sourceDir, "sourceDir");
			run();
		} finally {
			cleanup();
		}
	}

	public void rewrite(final Path sourceDir, final Path outputDir) throws Exception {
		if (this.sourceDir != null)
			throw new IllegalStateException("Instance is currently processing: " + this.sourceDir);

		try {
			this.sourceDir = Objects.requireNonNull(sourceDir, "sourceDir");
			this.outputDir = Objects.requireNonNull(outputDir, "outputDir");
			run();
		} finally {
			cleanup();
		}
	}

	private void run() throws Exception {
		final ASTParser parser = ASTParser.newParser(AST.JLS10);

		// Set Java version
		final Map<String, String> options = JavaCore.getOptions();
		JavaCore.setComplianceOptions(sourceCompatibility, options);
		parser.setCompilerOptions(options);

		// Collect processor flags
		int flags = 0;
		for (final SourceProcessor processor : processors)
			flags |= processor.getFlags();

		if ((flags & SourceProcessor.FLAG_RESOLVE_BINDINGS) != 0) {
			// Resolve references
			parser.setResolveBindings(true);
			parser.setBindingsRecovery(true);
		}

		// Set environment
		final String[] sourcePath = toArray(this.sourcePath.stream());
		parser.setEnvironment(toArray(classPath.stream()), sourcePath, getEncodings(sourcePath), true);

		// Walk directory to find source files
		final String[] sourceFiles = toArray(
				Files.walk(sourceDir).filter(p -> p.getFileName().toString().endsWith(JAVA_EXTENSION)));

		for (final SourceProcessor processor : processors)
			processor.initialize(this);

		// Parse source files
		parser.createASTs(sourceFiles, getEncodings(sourceFiles), EMPTY_STRING_ARRAY, requestor, null);

		for (final SourceProcessor processor : processors)
			processor.finish(this);
	}

	public void setEncoding(final Charset encoding) {
		this.encoding = Objects.requireNonNull(encoding, "encoding");
	}

	public void setSourceCompatibility(final String sourceCompatibility) {
		this.sourceCompatibility = Objects.requireNonNull(sourceCompatibility, "sourceCompatibility");
	}

}
