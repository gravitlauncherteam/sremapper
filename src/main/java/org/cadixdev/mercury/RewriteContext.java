/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury;

import static org.cadixdev.mercury.Mercury.JAVA_EXTENSION;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import org.cadixdev.mercury.jdt.rewrite.imports.ImportRewrite;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;

public final class RewriteContext extends SourceContext {

	private static TextEdit combineEdit(final TextEdit before, final TextEdit edit) {
		if (before == null)
			return edit;
		if (edit != null)
			before.addChild(edit);
		return before;
	}

	private TextEdit edit;
	private ImportRewrite importRewrite;

	private ASTRewrite rewrite;

	RewriteContext(final Mercury mercury, final Path sourceFile, final CompilationUnit compilationUnit,
			final String primaryType) {
		super(mercury, sourceFile, compilationUnit, primaryType);
	}

	public void addEdit(final TextEdit edit) {
		if (this.edit == null)
			this.edit = new MultiTextEdit();

		this.edit.addChild(Objects.requireNonNull(edit, "edit"));
	}

	public ASTRewrite createASTRewrite() {
		if (rewrite == null)
			rewrite = ASTRewrite.create(getCompilationUnit().getAST());
		return rewrite;
	}

	public ImportRewrite createImportRewrite() {
		if (importRewrite == null)
			importRewrite = ImportRewrite.create(getCompilationUnit(), true);
		return importRewrite;
	}

	public Optional<ASTRewrite> getASTRewrite() {
		return Optional.ofNullable(rewrite);
	}

	@Override
	void process(final List<SourceProcessor> processors) throws Exception {
		super.process(processors);

		final Path outputDir = getMercury().getOutputDir();

		String path = primaryType + JAVA_EXTENSION;
		if (!packageName.isEmpty()) {
			final StringJoiner joiner = new StringJoiner(outputDir.getFileSystem().getSeparator());

			for (final String part : packageName.split("\\."))
				joiner.add(part);
			joiner.add(path);

			path = joiner.toString();
		}

		final Path outputFile = outputDir.resolve(path);
		Files.createDirectories(outputFile.getParent());

		final TextEdit edit = rewrite();
		if (edit == null) {
			// Copy original source file
			Files.copy(getSourceFile(), outputFile, StandardCopyOption.REPLACE_EXISTING);
			return;
		}

		// Save the rewritten source file
		final Document document = loadDocument();
		edit.apply(document, TextEdit.NONE);

		try (OutputStreamWriter writer = new OutputStreamWriter(Files.newOutputStream(outputFile),
				getMercury().getEncoding())) {
			writer.write(document.get());
		}
	}

	private TextEdit rewrite() throws CoreException, IOException {
		if (rewrite == null && importRewrite == null && edit == null)
			return null;

		TextEdit edit = null;
		if (rewrite != null)
			edit = rewrite.rewriteAST(loadDocument(), null);

		if (importRewrite != null)
			edit = combineEdit(edit, importRewrite.rewriteImports(loadDocument(), null));

		return combineEdit(edit, this.edit);
	}

	public void setPackageName(final String packageName) {
		this.packageName = packageName != null ? packageName : "";
	}

	public void setPrimaryType(final String primaryType) {
		this.primaryType = Objects.requireNonNull(primaryType, "primaryType");
	}

}
