/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury.remapper;

import java.util.Objects;

import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.mercury.RewriteContext;
import org.cadixdev.mercury.SourceRewriter;

public final class MercuryRemapper implements SourceRewriter {

	public static SourceRewriter create(final MappingSet mappings) {
		return new MercuryRemapper(mappings, false);
	}

	public static SourceRewriter createSimple(final MappingSet mappings) {
		return new MercuryRemapper(mappings, true);
	}

	private final MappingSet mappings;
	private final boolean simple;

	private MercuryRemapper(final MappingSet mappings, final boolean simple) {
		this.mappings = Objects.requireNonNull(mappings, "mappings");
		this.simple = simple;
	}

	@Override
	public int getFlags() {
		return FLAG_RESOLVE_BINDINGS;
	}

	@Override
	public void rewrite(final RewriteContext context) {
		context.getCompilationUnit()
				.accept(simple ? new SimpleRemapperVisitor(context, mappings) : new RemapperVisitor(context, mappings));
	}

}
