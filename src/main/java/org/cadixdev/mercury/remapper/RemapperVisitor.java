/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury.remapper;

import static org.cadixdev.mercury.util.BombeBindings.isPackagePrivate;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.model.ClassMapping;
import org.cadixdev.lorenz.model.InnerClassMapping;
import org.cadixdev.lorenz.model.Mapping;
import org.cadixdev.lorenz.model.TopLevelClassMapping;
import org.cadixdev.mercury.RewriteContext;
import org.cadixdev.mercury.jdt.rewrite.imports.ImportRewrite;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;

class RemapperVisitor extends SimpleRemapperVisitor {

	private static class ImportContext extends ImportRewrite.ImportRewriteContext {
		final Set<String> conflicts;
		private final ImportRewrite.ImportRewriteContext defaultContext;
		final Map<String, String> implicit;

		ImportContext(final ImportRewrite.ImportRewriteContext defaultContext, final ImportContext parent) {
			this.defaultContext = defaultContext;
			if (parent != null) {
				implicit = new HashMap<>(parent.implicit);
				conflicts = new HashSet<>(parent.conflicts);
			} else {
				implicit = new HashMap<>();
				conflicts = new HashSet<>();
			}
		}

		@Override
		public int findInContext(final String qualifier, final String name, final int kind) {
			final int result = defaultContext.findInContext(qualifier, name, kind);
			if (result != RES_NAME_UNKNOWN)
				return result;

			if (kind == KIND_TYPE) {
				final String current = implicit.get(name);
				if (current != null)
					return current.equals(qualifier + '.' + name) ? RES_NAME_FOUND : RES_NAME_CONFLICT;

				if (conflicts.contains(name))
					return RES_NAME_CONFLICT; // TODO
			}

			return RES_NAME_UNKNOWN;
		}
	}

	private final ImportRewrite importRewrite;

	private final Deque<ImportContext> importStack = new ArrayDeque<>();

	RemapperVisitor(final RewriteContext context, final MappingSet mappings) {
		super(context, mappings);

		importRewrite = context.createImportRewrite();
		importRewrite.setUseContextToFilterImplicitImports(true);

		final TopLevelClassMapping primary = mappings.getTopLevelClassMapping(context.getQualifiedPrimaryType())
				.orElse(null);
		if (primary != null) {
			context.setPackageName(primary.getDeobfuscatedPackage().replace('/', '.'));
			importRewrite.setImplicitPackageName(context.getPackageName());

			final String simpleDeobfuscatedName = primary.getSimpleDeobfuscatedName();
			context.setPrimaryType(simpleDeobfuscatedName);

			final List<String> implicitTypes = new ArrayList<>();
			final String simpleObfuscatedName = primary.getSimpleObfuscatedName();

			@SuppressWarnings("unchecked")
			final List<AbstractTypeDeclaration> types = context.getCompilationUnit().types();
			for (final AbstractTypeDeclaration type : types) {
				final String name = type.getName().getIdentifier();
				if (name.equals(simpleObfuscatedName))
					implicitTypes.add(simpleDeobfuscatedName);
				else
					implicitTypes.add(mappings.getTopLevelClassMapping(context.getPackageName() + '.' + name)
							.map(Mapping::getSimpleDeobfuscatedName).orElse(name));
			}
			importRewrite.setImplicitTypes(implicitTypes);
		}
	}

	private void collectImportContext(final ImportContext context, final ITypeBinding binding) {
		if (binding == null)
			return;

		// Names from inner classes
		for (final ITypeBinding inner : binding.getDeclaredTypes()) {
			if (inner.getBinaryName() == null)
				continue;

			final int modifiers = inner.getModifiers();
			if (Modifier.isPrivate(modifiers))
				// Inner type must be declared in this compilation unit
				if (this.context.getCompilationUnit().findDeclaringNode(inner) == null)
					continue;

			final ClassMapping<?, ?> mapping = mappings.getClassMapping(inner.getBinaryName()).orElse(null);

			if (isPackagePrivate(modifiers)) {
				// Must come from the same package
				final String packageName = mapping != null ? mapping.getDeobfuscatedPackage()
						: inner.getPackage().getName();
				if (packageName.equals(this.context.getPackageName()))
					continue;
			}

			String simpleName;
			String qualifiedName;
			if (mapping != null) {
				simpleName = mapping.getSimpleDeobfuscatedName();
				qualifiedName = mapping.getFullDeobfuscatedName().replace('/', '.').replace('$', '.');
			} else {
				simpleName = inner.getName();
				qualifiedName = inner.getBinaryName().replace('$', '.');
			}

			if (!context.conflicts.contains(simpleName)) {
				final String current = context.implicit.putIfAbsent(simpleName, qualifiedName);
				if (current != null && !current.equals(qualifiedName)) {
					context.implicit.remove(simpleName);
					context.conflicts.add(simpleName);
				}
			}
		}

		// Inherited names
		collectImportContext(context, binding.getSuperclass());
		for (final ITypeBinding parent : binding.getInterfaces())
			collectImportContext(context, parent);
	}

	@Override
	public void endVisit(final AnnotationTypeDeclaration node) {
		importStack.pop();
	}

	@Override
	public void endVisit(final AnonymousClassDeclaration node) {
		importStack.pop();
	}

	@Override
	public void endVisit(final EnumDeclaration node) {
		importStack.pop();
	}

	@Override
	public void endVisit(final TypeDeclaration node) {
		importStack.pop();
	}

	private void pushImportContext(final ITypeBinding binding) {
		final ImportContext context = new ImportContext(importRewrite.getDefaultImportRewriteContext(),
				importStack.peek());
		collectImportContext(context, binding);
		importStack.push(context);
	}

	private void remapInnerType(final QualifiedName qualifiedName, final ITypeBinding outerClass) {
		if (outerClass.getBinaryName() == null)
			return;

		final ClassMapping<?, ?> outerClassMapping = mappings.computeClassMapping(outerClass.getBinaryName())
				.orElse(null);
		if (outerClassMapping == null)
			return;

		final SimpleName node = qualifiedName.getName();
		final InnerClassMapping mapping = outerClassMapping.getInnerClassMapping(node.getIdentifier()).orElse(null);
		if (mapping == null)
			return;

		updateIdentifier(node, mapping.getDeobfuscatedName());
	}

	private void remapQualifiedType(final QualifiedName node, final ITypeBinding binding) {
		final String binaryName = binding.getBinaryName();

		if (binaryName == null)
			return;

		final TopLevelClassMapping mapping = mappings.getTopLevelClassMapping(binaryName).orElse(null);

		if (mapping == null)
			return;

		final String newName = mapping.getDeobfuscatedName().replace('/', '.');
		if (binaryName.equals(newName))
			return;

		context.createASTRewrite().replace(node, node.getAST().newName(newName), null);
	}

	private void remapType(final SimpleName node, final ITypeBinding binding) {
		if (binding.isTypeVariable() || binding.getBinaryName() == null)
			return;

		final ClassMapping<?, ?> mapping = mappings.computeClassMapping(binding.getBinaryName()).orElse(null);

		if (node.getParent() instanceof AbstractTypeDeclaration || binding.isLocal()) {
			if (mapping != null)
				updateIdentifier(node, mapping.getSimpleDeobfuscatedName());
			return;
		}

		final String qualifiedName = (mapping != null ? mapping.getFullDeobfuscatedName().replace('/', '.')
				: binding.getBinaryName()).replace('$', '.');
		final String newName = importRewrite.addImport(qualifiedName, importStack.peek());

		if (!node.getIdentifier().equals(newName))
			if (newName.indexOf('.') == -1)
				context.createASTRewrite().set(node, SimpleName.IDENTIFIER_PROPERTY, newName, null);
			else
				// Qualified name
				context.createASTRewrite().replace(node, node.getAST().newName(newName), null);
	}

	@Override
	public boolean visit(final AnnotationTypeDeclaration node) {
		pushImportContext(node.resolveBinding());
		return true;
	}

	@Override
	public boolean visit(final AnonymousClassDeclaration node) {
		pushImportContext(node.resolveBinding());
		return true;
	}

	@Override
	public boolean visit(final EnumDeclaration node) {
		pushImportContext(node.resolveBinding());
		return true;
	}

	@Override
	public boolean visit(final ImportDeclaration node) {
		final IBinding binding = node.resolveBinding();
		if (binding != null)
			switch (binding.getKind()) {
			case IBinding.TYPE:
				final ITypeBinding typeBinding = (ITypeBinding) binding;
				final String name = typeBinding.getBinaryName();
				if (name != null) {
					final ClassMapping<?, ?> mapping = mappings.computeClassMapping(name).orElse(null);
					if (mapping != null && !name.equals(mapping.getFullDeobfuscatedName().replace('/', '.')))
						importRewrite.removeImport(typeBinding.getQualifiedName());
				}

				break;
			}
		return false;
	}

	@Override
	public boolean visit(final PackageDeclaration node) {
		final String currentPackage = node.getName().getFullyQualifiedName();

		if (!currentPackage.equals(context.getPackageName()))
			context.createASTRewrite().replace(node.getName(), node.getAST().newName(context.getPackageName()), null);

		return false;
	}

	@Override
	public boolean visit(final QualifiedName node) {
		final IBinding binding = node.resolveBinding();
		if (binding == null)
			return false;

		if (binding.getKind() != IBinding.TYPE)
			// Unpack the qualified name and remap method/field and type separately
			return true;

		final Name qualifier = node.getQualifier();
		final IBinding qualifierBinding = qualifier.resolveBinding();
		switch (qualifierBinding.getKind()) {
		case IBinding.PACKAGE:
			// Remap full qualified type
			remapQualifiedType(node, (ITypeBinding) binding);
			break;
		case IBinding.TYPE:
			// Remap inner type separately
			remapInnerType(node, (ITypeBinding) qualifierBinding);

			// Remap the qualifier
			qualifier.accept(this);
			break;
		default:
			throw new IllegalStateException("Unexpected qualifier binding: " + binding.getClass().getSimpleName() + " ("
					+ binding.getKind() + ')');
		}

		return false;
	}

	@Override
	protected void visit(final SimpleName node, final IBinding binding) {
		switch (binding.getKind()) {
		case IBinding.TYPE:
			remapType(node, (ITypeBinding) binding);
			break;
		case IBinding.METHOD:
		case IBinding.VARIABLE:
			super.visit(node, binding);
			break;
		case IBinding.PACKAGE:
			// This is ignored because it should be covered by separate handling
			// of QualifiedName (for full-qualified class references),
			// PackageDeclaration and ImportDeclaration
		default:
			throw new IllegalStateException(
					"Unhandled binding: " + binding.getClass().getSimpleName() + " (" + binding.getKind() + ')');
		}
	}

	@Override
	public boolean visit(final TypeDeclaration node) {
		pushImportContext(node.resolveBinding());
		return true;
	}

}
