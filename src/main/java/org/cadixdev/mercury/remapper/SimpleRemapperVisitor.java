/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury.remapper;

import static org.cadixdev.mercury.util.BombeBindings.convertSignature;

import org.cadixdev.bombe.analysis.InheritanceProvider;
import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.model.ClassMapping;
import org.cadixdev.lorenz.model.FieldMapping;
import org.cadixdev.lorenz.model.MethodMapping;
import org.cadixdev.mercury.RewriteContext;
import org.cadixdev.mercury.analysis.MercuryInheritanceProvider;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.SimpleName;

/**
 * Remaps only methods and fields.
 */
class SimpleRemapperVisitor extends ASTVisitor {

	final RewriteContext context;
	private final InheritanceProvider inheritanceProvider;
	final MappingSet mappings;

	SimpleRemapperVisitor(final RewriteContext context, final MappingSet mappings) {
		this.context = context;
		this.mappings = mappings;
		inheritanceProvider = MercuryInheritanceProvider.get(context.getMercury());
	}

	private void remapField(final SimpleName node, final IVariableBinding binding) {
		if (!binding.isField())
			return;

		final ITypeBinding declaringClass = binding.getDeclaringClass();
		if (declaringClass == null || declaringClass.getBinaryName() == null)
			return;

		final ClassMapping<?, ?> classMapping = mappings.getClassMapping(declaringClass.getBinaryName()).orElse(null);
		if (classMapping == null)
			return;

		final FieldMapping mapping = classMapping.computeFieldMapping(convertSignature(binding)).orElse(null);
		if (mapping == null)
			return;

		updateIdentifier(node, mapping.getDeobfuscatedName());
	}

	private void remapMethod(final SimpleName node, final IMethodBinding binding) {
		final ITypeBinding declaringClass = binding.getDeclaringClass();

		if (declaringClass.getBinaryName() == null)
			return;

		final ClassMapping<?, ?> classMapping = mappings.getOrCreateClassMapping(declaringClass.getBinaryName());

		if (binding.isConstructor())
			updateIdentifier(node, classMapping.getSimpleDeobfuscatedName());
		else {
			classMapping.complete(inheritanceProvider, declaringClass);

			final MethodMapping mapping = classMapping.getMethodMapping(convertSignature(binding)).orElse(null);
			if (mapping == null)
				return;

			updateIdentifier(node, mapping.getDeobfuscatedName());
		}
	}

	final void updateIdentifier(final SimpleName node, final String newName) {
		if (!node.getIdentifier().equals(newName))
			context.createASTRewrite().set(node, SimpleName.IDENTIFIER_PROPERTY, newName, null);
	}

	@Override
	public final boolean visit(final SimpleName node) {
		final IBinding binding = node.resolveBinding();
		if (binding != null)
			visit(node, binding);
		return false;
	}

	protected void visit(final SimpleName node, final IBinding binding) {
		switch (binding.getKind()) {
		case IBinding.METHOD:
			remapMethod(node, ((IMethodBinding) binding).getMethodDeclaration());
			break;
		case IBinding.VARIABLE:
			remapField(node, ((IVariableBinding) binding).getVariableDeclaration());
			break;
		}
	}

}
