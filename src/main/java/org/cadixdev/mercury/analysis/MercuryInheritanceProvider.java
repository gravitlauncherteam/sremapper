/*
 * Copyright (c) 2018 Minecrell (https://github.com/Minecrell)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which accompanies this distribution,
 * and is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.cadixdev.mercury.analysis;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.cadixdev.bombe.analysis.CachingInheritanceProvider;
import org.cadixdev.bombe.analysis.InheritanceProvider;
import org.cadixdev.bombe.analysis.InheritanceType;
import org.cadixdev.bombe.type.signature.FieldSignature;
import org.cadixdev.bombe.type.signature.MethodSignature;
import org.cadixdev.mercury.Mercury;
import org.cadixdev.mercury.util.BombeBindings;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;

public class MercuryInheritanceProvider implements InheritanceProvider {

	private static class BindingClassInfo extends Abstract {

		private static String getInternalName(final ITypeBinding binding) {
			return binding.getBinaryName().replace('.', '/');
		}

		private final ITypeBinding binding;

		private BindingClassInfo(final ITypeBinding binding) {
			this.binding = binding;
		}

		@Override
		public Map<FieldSignature, InheritanceType> getFields() {
			return Collections.unmodifiableMap(Arrays.stream(binding.getDeclaredFields()).collect(Collectors
					.toMap(BombeBindings::convertSignature, f -> InheritanceType.fromModifiers(f.getModifiers()))));
		}

		@Override
		public Map<String, InheritanceType> getFieldsByName() {
			return Collections.unmodifiableMap(Arrays.stream(binding.getDeclaredFields()).collect(
					Collectors.toMap(IVariableBinding::getName, f -> InheritanceType.fromModifiers(f.getModifiers()))));
		}

		@Override
		public List<String> getInterfaces() {
			return Collections.unmodifiableList(Arrays.stream(binding.getInterfaces())
					.map(BindingClassInfo::getInternalName).collect(Collectors.toList()));
		}

		@Override
		public Map<MethodSignature, InheritanceType> getMethods() {
			return Collections.unmodifiableMap(Arrays.stream(binding.getDeclaredMethods()).collect(Collectors
					.toMap(BombeBindings::convertSignature, m -> InheritanceType.fromModifiers(m.getModifiers()), (a, b) -> a, HashMap::new)));
		}

		@Override
		public String getName() {
			return getInternalName(binding);
		}

		@Override
		public String getSuperName() {
			final ITypeBinding superClass = binding.getSuperclass();
			return superClass != null ? getInternalName(superClass) : "";
		}

		@Override
		public boolean isInterface() {
			return binding.isInterface();
		}

		private void provideParent(final InheritanceProvider provider, final ITypeBinding parent,
				final Collection<ClassInfo> parents) {
			if (parent == null)
				return;

			final ClassInfo parentInfo = provider.provide(getInternalName(parent), parent).orElse(null);
			if (parentInfo != null) {
				parentInfo.provideParents(provider, parents);
				parents.add(parentInfo);
			}
		}

		@Override
		public void provideParents(final InheritanceProvider provider, final Collection<ClassInfo> parents) {
			provideParent(provider, binding.getSuperclass(), parents);
			for (final ITypeBinding iface : binding.getInterfaces())
				provideParent(provider, iface, parents);
		}

	}

	public static InheritanceProvider get(final Mercury mercury) {
		return (InheritanceProvider) mercury.getContext().computeIfAbsent(InheritanceProvider.class,
				i -> new CachingInheritanceProvider(new MercuryInheritanceProvider(mercury)));
	}

	private final Mercury mercury;

	private MercuryInheritanceProvider(final Mercury mercury) {
		this.mercury = mercury;
	}

	public ClassInfo provide(final ITypeBinding binding) {
		return new BindingClassInfo(binding.getErasure()).lazy();
	}

	@Override
	public Optional<ClassInfo> provide(final String klass) {
		return mercury.createTypeBinding(klass).map(this::provide);
	}

	@Override
	public Optional<ClassInfo> provide(final String klass, final Object context) {
		if (context instanceof ITypeBinding)
			// Avoid looking up binding if it is provided in context
			return Optional.of(provide((ITypeBinding) context));
		else
			return provide(klass);
	}

}
