/*
 * This file is part of Lorenz, licensed under the MIT License (MIT).
 *
 * Copyright (c) Jamie Mansfield <https://www.jamierocks.uk/>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.lorenz.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.MappingSetModelFactory;
import org.cadixdev.lorenz.model.TopLevelClassMapping;
import org.cadixdev.lorenz.model.jar.CascadingFieldTypeProvider;

/**
 * A basic implementation of {@link MappingSet}.
 *
 * @author Jamie Mansfield
 * @since 0.2.0
 */
public class MappingSetImpl implements MappingSet {

	private final CascadingFieldTypeProvider fieldTypeProvider = new CascadingFieldTypeProvider();
	private final MappingSetModelFactory modelFactory;
	private final Map<String, TopLevelClassMapping> topLevelClasses = new HashMap<>();

	public MappingSetImpl() {
		this(new MappingSetModelFactoryImpl());
	}

	public MappingSetImpl(final MappingSetModelFactory modelFactory) {
		this.modelFactory = modelFactory;
	}

	@Override
	public TopLevelClassMapping createTopLevelClassMapping(final String obfuscatedName, final String deobfuscatedName) {
		return topLevelClasses.compute(obfuscatedName.replace('.', '/'), (name, existingMapping) -> {
			if (existingMapping != null)
				return existingMapping.setDeobfuscatedName(deobfuscatedName);
			return getModelFactory().createTopLevelClassMapping(this, name, deobfuscatedName);
		});
	}

	@Override
	public CascadingFieldTypeProvider getFieldTypeProvider() {
		return fieldTypeProvider;
	}

	@Override
	public MappingSetModelFactory getModelFactory() {
		return modelFactory;
	}

	@Override
	public Optional<TopLevelClassMapping> getTopLevelClassMapping(final String obfuscatedName) {
		return Optional.ofNullable(topLevelClasses.get(obfuscatedName.replace('.', '/')));
	}

	@Override
	public Collection<TopLevelClassMapping> getTopLevelClassMappings() {
		return Collections.unmodifiableCollection(topLevelClasses.values());
	}

	@Override
	public boolean hasTopLevelClassMapping(final String obfuscatedName) {
		return topLevelClasses.containsKey(obfuscatedName.replace('.', '/'));
	}

}
