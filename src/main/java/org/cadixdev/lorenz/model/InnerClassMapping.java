/*
 * This file is part of Lorenz, licensed under the MIT License (MIT).
 *
 * Copyright (c) Jamie Mansfield <https://www.jamierocks.uk/>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.lorenz.model;

/**
 * Represents a de-obfuscation mapping for an inner class.
 *
 * @author Jamie Mansfield
 * @since 0.1.0
 */
@SuppressWarnings("rawtypes")
public interface InnerClassMapping
		extends ClassMapping<InnerClassMapping, ClassMapping>, MemberMapping<InnerClassMapping, ClassMapping> {

	@Override
	default InnerClassMapping copy(final ClassMapping parent) {
		final InnerClassMapping mapping = parent.createInnerClassMapping(getObfuscatedName(), getDeobfuscatedName());
		getFieldMappings().forEach(field -> field.copy(mapping));
		getMethodMappings().forEach(method -> method.copy(mapping));
		getInnerClassMappings().forEach(klass -> klass.copy(mapping));
		return mapping;
	}

	@Override
	default String getDeobfuscatedPackage() {
		return getParent().getDeobfuscatedPackage();
	}

	@Override
	default String getFullDeobfuscatedName() {
		return String.format("%s$%s", getParent().getFullDeobfuscatedName(), getDeobfuscatedName());
	}

	@Override
	default String getFullObfuscatedName() {
		return String.format("%s$%s", getParent().getFullObfuscatedName(), getObfuscatedName());
	}

	@Override
	default String getObfuscatedPackage() {
		return getParent().getObfuscatedPackage();
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * <strong>Note:</strong> The simple name is empty for anonymous classes. For
	 * local classes, the leading digits are stripped.
	 * </p>
	 */
	@Override
	String getSimpleDeobfuscatedName();

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * <strong>Note:</strong> The simple name is empty for anonymous classes. For
	 * local classes, the leading digits are stripped.
	 * </p>
	 */
	@Override
	String getSimpleObfuscatedName();

	@Override
	default InnerClassMapping merge(final InnerClassMapping with, final ClassMapping parent) {
		// create the container mapping
		final InnerClassMapping newMapping = parent.getOrCreateInnerClassMapping(getObfuscatedName())
				.setDeobfuscatedName(with.getDeobfuscatedName());

		// fill with child data
		getFieldMappings().forEach(field -> {
			final FieldMapping fieldWith = with.getOrCreateFieldMapping(field.getDeobfuscatedSignature());
			field.merge(fieldWith, newMapping);
		});
		getMethodMappings().forEach(method -> {
			final MethodMapping methodWith = with.getOrCreateMethodMapping(method.getDeobfuscatedSignature());
			method.merge(methodWith, newMapping);
		});
		getInnerClassMappings().forEach(klass -> {
			final InnerClassMapping klassWith = with.getOrCreateInnerClassMapping(klass.getDeobfuscatedName());
			klass.merge(klassWith, newMapping);
		});

		// A -> [B / C] -> D
		return newMapping;
	}

	@Override
	default InnerClassMapping reverse(final ClassMapping parent) {
		final InnerClassMapping mapping = parent.createInnerClassMapping(getDeobfuscatedName(), getObfuscatedName());
		getFieldMappings().forEach(field -> field.reverse(mapping));
		getMethodMappings().forEach(method -> method.reverse(mapping));
		getInnerClassMappings().forEach(klass -> klass.reverse(mapping));
		return mapping;
	}

	/**
	 * Sets the de-obfuscated name of this inner class mapping.
	 *
	 * <em>Implementations will need to support the input being a fully-qualified
	 * name!</em>
	 *
	 * @param deobfuscatedName The new de-obfuscated name
	 * @return {@code this}, for chaining
	 */
	@Override
	InnerClassMapping setDeobfuscatedName(final String deobfuscatedName);

}
