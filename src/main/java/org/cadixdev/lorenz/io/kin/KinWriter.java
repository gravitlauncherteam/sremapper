/*
 * This file is part of Lorenz, licensed under the MIT License (MIT).
 *
 * Copyright (c) Jamie Mansfield <https://www.jamierocks.uk/>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.lorenz.io.kin;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;
import java.util.zip.GZIPOutputStream;

import org.cadixdev.bombe.type.FieldType;
import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.io.BinaryMappingsWriter;
import org.cadixdev.lorenz.io.MappingsWriter;
import org.cadixdev.lorenz.model.ClassMapping;
import org.cadixdev.lorenz.model.FieldMapping;
import org.cadixdev.lorenz.model.InnerClassMapping;
import org.cadixdev.lorenz.model.MethodMapping;
import org.cadixdev.lorenz.model.TopLevelClassMapping;

/**
 * An implementation of {@link MappingsWriter} for the Kin format.
 *
 * @author Jamie Mansfield
 * @since 0.4.0
 */
public class KinWriter extends BinaryMappingsWriter {

	public KinWriter(final OutputStream stream) throws IOException {
		super(new GZIPOutputStream(stream));
	}

	@Override
	public void write(final MappingSet mappings) throws IOException {
		stream.writeInt(KinConstants.MAGIC);
		stream.writeByte(KinConstants.VERSION_ONE);

		// fake package info
		stream.writeInt(0);

		// write classes
		stream.writeInt(mappings.getTopLevelClassMappings().size());
		for (final TopLevelClassMapping klass : mappings.getTopLevelClassMappings())
			writeClass(klass);
	}

	private void writeClass(final ClassMapping<?, ?> mapping) throws IOException {
		stream.writeUTF(mapping.getObfuscatedName());
		stream.writeUTF(mapping.getDeobfuscatedName());

		// write fields
		stream.writeInt(mapping.getFieldMappings().size());
		for (final FieldMapping field : mapping.getFieldMappings()) {
			stream.writeUTF(field.getObfuscatedName());
			final Optional<FieldType> type = field.getType();
			stream.writeBoolean(type.isPresent());
			if (type.isPresent())
				stream.writeUTF(mapping.getMappings().deobfuscate(type.get()).toString());
			stream.writeUTF(field.getDeobfuscatedName());
		}

		// write methods
		stream.writeInt(mapping.getMethodMappings().size());
		for (final MethodMapping method : mapping.getMethodMappings()) {
			stream.writeUTF(method.getObfuscatedName());
			stream.writeUTF(method.getObfuscatedDescriptor());
			stream.writeUTF(method.getDeobfuscatedName());
		}

		// write inner classes
		stream.writeInt(mapping.getInnerClassMappings().size());
		for (final InnerClassMapping inner : mapping.getInnerClassMappings())
			writeClass(inner);
	}

}
