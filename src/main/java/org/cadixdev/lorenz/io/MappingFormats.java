/*
 * This file is part of Lorenz, licensed under the MIT License (MIT).
 *
 * Copyright (c) Jamie Mansfield <https://www.jamierocks.uk/>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.cadixdev.lorenz.io;

import java.util.ServiceLoader;

import org.cadixdev.lorenz.util.Registry;

/**
 * A psuedo-enum of the mapping formats implemented within Lorenz.
 *
 * @author Jamie Mansfield
 * @since 0.4.0
 */
public final class MappingFormats {

	/**
	 * The CSRG (compact SRG) mapping format.
	 */
	public static final TextMappingFormat CSRG;

	/**
	 * The Engima mapping format.
	 */
	public static final TextMappingFormat ENGIMA;

	/**
	 * The JAM mapping format.
	 */
	public static final TextMappingFormat JAM;

	/**
	 * The KIN mapping format.
	 */
	public static final MappingFormat KIN;

	/**
	 * The registry of {@link MappingFormat}s.
	 */
	public static final Registry<MappingFormat> REGISTRY;

	/**
	 * The SRG mapping format.
	 */
	public static final TextMappingFormat SRG;

	/**
	 * The TSRG (tiny SRG) mapping format.
	 */
	public static final TextMappingFormat TSRG;
	
	/**
	 * The ProGuard mapping format.
	 */
	public static final TextMappingFormat PROGUARD;

	/**
	 * The Yarn mapping format.
	 */
	public static final TextMappingFormat YARN;

	static {
		REGISTRY = new Registry<>();
		// Populate the registry
		for (final MappingFormat format : ServiceLoader.load(MappingFormat.class))
			REGISTRY.register(format.toString(), format);
		SRG = (TextMappingFormat) byId("srg");
		TSRG = (TextMappingFormat) byId("tsrg");
		CSRG = (TextMappingFormat) byId("csrg");
		ENGIMA = (TextMappingFormat) byId("enigma");
		JAM = (TextMappingFormat) byId("jam");
		KIN = byId("kin");
		PROGUARD = (TextMappingFormat) byId("proguard");
		YARN = (TextMappingFormat) byId("yarn");
	}

	/**
	 * @see Registry#byId(String)
	 */
	public static MappingFormat byId(final String id) {
		return REGISTRY.byId(id);
	}

	private MappingFormats() {
	}

}
