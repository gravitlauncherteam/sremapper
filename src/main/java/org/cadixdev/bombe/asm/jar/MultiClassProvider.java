package org.cadixdev.bombe.asm.jar;

import java.util.List;

public class MultiClassProvider implements ClassProvider {
	private final List<ClassProvider> providers;

	public MultiClassProvider(final List<ClassProvider> providers) {
		this.providers = providers;
	}

	@Override
	public byte[] get(final String klass) {
		byte[] ret;
		for (final ClassProvider p : providers) {
			ret = p.get(klass);
			if (ret != null)
				return ret;
		}
		return null;
	}

	public List<ClassProvider> getProviders() {
		return providers;
	}
}
