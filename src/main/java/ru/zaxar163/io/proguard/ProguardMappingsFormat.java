package ru.zaxar163.io.proguard;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Optional;

import org.cadixdev.lorenz.io.MappingsReader;
import org.cadixdev.lorenz.io.MappingsWriter;
import org.cadixdev.lorenz.io.TextMappingFormat;

public final class ProguardMappingsFormat implements TextMappingFormat {
	public static final Optional<String> FILE_EXT = Optional.of("pro");

	@Override
	public Optional<String> getStandardFileExtension() {
		return FILE_EXT;
	}

	@Override
	public String toString() {
		return "proguard";
	}

	@Override
	public MappingsReader createReader(Reader reader) throws IOException {
		return new ProguardReader(reader);
	}

	@Override
	public MappingsWriter createWriter(Writer writer) throws IOException {
		throw new UnsupportedOperationException();
	}
}
