package ru.zaxar163.io.yarn;

import java.io.Reader;
import org.cadixdev.lorenz.io.TextMappingsReader;

class YarnReader extends TextMappingsReader {
	private static final int TAB = '\t';
	YarnReader(Reader reader) {
		super(reader, m -> new Processor(m) {
			String latestClassDecl;
			@Override
			public void accept(String t) {
				long level = t.chars().filter(e -> e == TAB).count();
				String[] parts = SPACE.split(t);
				if (level > 1) return;
				if (level == 1 && parts.length > 3) {
					if (parts[0].equalsIgnoreCase("METHOD")) {
						mappings.getOrCreateClassMapping(latestClassDecl).getOrCreateMethodMapping(parts[1], parts[3]).setDeobfuscatedName(parts[2]);
					} else if (parts[0].equalsIgnoreCase("FIELD")) {
						mappings.getOrCreateClassMapping(latestClassDecl).getOrCreateFieldMapping(parts[1], parts[3]).setDeobfuscatedName(parts[2]);
					}
				} else if (level == 0 && parts[0].equalsIgnoreCase("CLASS") && parts.length > 2) {
					latestClassDecl = parts[1];
					mappings.getOrCreateClassMapping(parts[1]).setDeobfuscatedName(parts[2]);
				}
			}
		});
	}
}
