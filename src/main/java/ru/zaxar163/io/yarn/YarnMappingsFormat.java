package ru.zaxar163.io.yarn;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Optional;

import org.cadixdev.lorenz.io.MappingsReader;
import org.cadixdev.lorenz.io.MappingsWriter;
import org.cadixdev.lorenz.io.TextMappingFormat;

public class YarnMappingsFormat implements TextMappingFormat {
	public static final Optional<String> STD_EXC = Optional.of("mapping");
	
	@Override
	public Optional<String> getStandardFileExtension() {
		return STD_EXC;
	}

	@Override
	public MappingsReader createReader(Reader reader) throws IOException {
		return new YarnReader(reader);
	}

	@Override
	public MappingsWriter createWriter(Writer writer) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "yarn";
	}
}
