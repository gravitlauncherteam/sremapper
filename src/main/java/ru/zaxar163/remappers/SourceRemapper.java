package ru.zaxar163.remappers;

import java.nio.file.Path;
import java.util.List;

import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.mercury.Mercury;
import org.cadixdev.mercury.at.AccessTransformerRewriter;
import org.cadixdev.mercury.extra.BridgeMethodRewriter;
import org.cadixdev.mercury.remapper.MercuryRemapper;

public final class SourceRemapper {

	public static void process(final Path in, final Path out, final List<Path> libraries, final List<Path> srcPath,
			final MappingSet map, final AccessTransformSet acc, final boolean removeBridge) throws Throwable {
		final Mercury m = new Mercury();
		m.getClassPath().addAll(libraries);
		m.getSourcePath().addAll(srcPath);
		if (removeBridge)
			m.getProcessors().add(BridgeMethodRewriter.create());
		m.getProcessors().add(AccessTransformerRewriter.create(acc));
		m.getProcessors().add(MercuryRemapper.create(map));
		m.rewrite(in, out);
		Utils.clean();
	}

	private SourceRemapper() {
	}
}
