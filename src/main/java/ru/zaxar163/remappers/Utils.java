package ru.zaxar163.remappers;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.at.io.AccessTransformFormats;
import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.io.MappingFormats;

public final class Utils {

	public static final String AT = "at";

	public static final String CSRG = "csrg";

	public static final String ENGIMA = "eng";

	public static final String JAM = "jam";

	public static final String KIN = "kin";

	public static final String SRG = "srg";

	public static final String TSRG = "tsrg";

	public static void clean() {
		System.gc();
		System.runFinalization();
		System.gc();
	}

	public static MappingSet invert(final MappingSet mappings) {
		return mappings.reverse();
	}

	public static MappingSet merge(final MappingSet fromObf, final MappingSet toDeobf) {
		MappingSet out = MappingSet.create();
		fromObf.merge(toDeobf, out);
		return out;
	}

	public static void readMappings(final MappingSet map, final AccessTransformSet acc, final List<Path> mappings)
			throws IOException {
		for (final Path p : mappings)
			if (p.toString().endsWith(".srg"))
				MappingFormats.SRG.read(map, p);
			else if (p.toString().endsWith(".tsrg"))
				MappingFormats.TSRG.read(map, p);
			else if (p.toString().endsWith(".csrg"))
				MappingFormats.CSRG.read(map, p);
			else if (p.toString().endsWith(".eng"))
				MappingFormats.ENGIMA.read(map, p);
			else if (p.toString().endsWith(".jam"))
				MappingFormats.JAM.read(map, p);
			else if (p.toString().endsWith(".kin"))
				MappingFormats.KIN.read(map, p);
			else if (p.toString().endsWith(".mapping"))
				MappingFormats.YARN.read(map, p);
			else if (p.toString().endsWith(".pro"))
				MappingFormats.PROGUARD.read(map, p);
			else if (p.toString().endsWith(".at"))
				AccessTransformFormats.FML.read(p, acc);
	}

	public static void writeAT(final Path p, final AccessTransformSet acc) throws IOException {
		if (p.toString().endsWith(".at"))
			AccessTransformFormats.FML.write(p, acc);
	}

	public static void writeMappings(final Path p, final MappingSet map) throws IOException {
		if (p.toString().endsWith(".srg"))
			MappingFormats.SRG.write(map, p);
		else if (p.toString().endsWith(".tsrg"))
			MappingFormats.TSRG.write(map, p);
		else if (p.toString().endsWith(".csrg"))
			MappingFormats.CSRG.write(map, p);
		else if (p.toString().endsWith(".eng"))
			MappingFormats.ENGIMA.write(map, p);
		else if (p.toString().endsWith(".jam"))
			MappingFormats.JAM.write(map, p);
		else if (p.toString().endsWith(".kin"))
			MappingFormats.KIN.write(map, p);
	}

	private Utils() {
	}
}
