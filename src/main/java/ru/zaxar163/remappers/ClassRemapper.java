package ru.zaxar163.remappers;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarFile;

import org.cadixdev.bombe.asm.analysis.ClassProviderInheritanceProvider;
import org.cadixdev.bombe.asm.jar.ClassLoaderClassProvider;
import org.cadixdev.bombe.asm.jar.JarEntryRemappingTransformer;
import org.cadixdev.bombe.asm.jar.JarFileClassProvider;
import org.cadixdev.bombe.asm.jar.MultiClassProvider;
import org.cadixdev.bombe.jar.Jars;
import org.cadixdev.lorenz.MappingSet;
import org.cadixdev.lorenz.asm.LorenzRemapper;

public final class ClassRemapper {

	private static final class NULLClassLoader extends ClassLoader {
		private NULLClassLoader() {
			super(null);
		}
	}

	public static void process(final Path inputJar, final Path outputJar, final List<Path> libraries,
			final MappingSet mappings) throws Throwable {
		final MultiClassProvider p = new MultiClassProvider(new ArrayList<>());
		p.getProviders().add(new ClassLoaderClassProvider(new NULLClassLoader()));
		for (final Path jar : libraries)
			p.getProviders().add(new JarFileClassProvider(new JarFile(jar.toFile())));
		Jars.transform(inputJar, outputJar, new JarEntryRemappingTransformer(
				new LorenzRemapper(mappings, new ClassProviderInheritanceProvider(p))));
		Utils.clean();
	}

	private ClassRemapper() {
	}
}
