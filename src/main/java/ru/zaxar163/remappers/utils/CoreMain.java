package ru.zaxar163.remappers.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CoreMain {
	public static void main(String[] args) throws Throwable {
		if (args.length < 1) {
			System.out.println("Modes: yarnMerger; formatConverter; inverser; merger; jarRemapper; srcRemapper");
			System.exit(1);
		}
		String[] toMethod = Arrays.copyOfRange(args, 1, args.length);
		switch (args[0]) {
		case "yarnMerger":
			SimpleUtils.yarnmerge(toMethod);
			break;
		case "formatConverter":
			SimpleUtils.convert(toMethod);
			break;
		case "inverser":
			SimpleUtils.inverse(toMethod);
			break;
		case "merger":
			SimpleUtils.merge(toMethod);
			break;
		case "jarRemapper":
			Remappers.classRemap(parseArgs(toMethod));
			break;
		case "srcRemapper":
			Remappers.sourceRemap(parseArgs(toMethod));
			break;
		default:
			System.out.println("Unsupported mode: " + args[0]);
			break;
		}
	}
	
	public static Map<String, String> parseArgs(String[] args) {
		Map<String, String> ret = new HashMap<>();
		for (String arg : args)
			if (arg.startsWith("--")) {
				String argf = arg.substring(2);
				if (argf.contains("=")) {
					String[] split = argf.split("=");
					ret.put(split[0], split[1]);
				} else ret.put(argf, "true");
			}
		return ret;
	}
}
