package ru.zaxar163.remappers.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.stream.Stream;

import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.lorenz.MappingSet;

import ru.zaxar163.remappers.Utils;

public class SimpleUtils {
    private static final OpenOption[] WRITE_OPTIONS = {StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING};
	public static void yarnmerge(String[] args) throws IOException {
		if (args.length < 2) {
			System.out.println("Usage: <inDir> <out>\nMerges dir contains lots of yarn mappings into one file.");
			System.exit(1);
		}
		try (PrintWriter w = new PrintWriter(Files.newBufferedWriter(Paths.get(args[1]), StandardCharsets.UTF_8, WRITE_OPTIONS))) {
			Files.walk(Paths.get(args[0]), Integer.MAX_VALUE).forEach(e -> {
				if (e.toString().endsWith(".mapping") && !Files.isDirectory(e) && Files.isReadable(e))
					try(Stream<String> lines = Files.lines(e, StandardCharsets.UTF_8)) {
						lines.forEach(w::println);
					} catch (IOException t) {
						throw new RuntimeException(t);
					}
			});
		}
		Utils.clean();
	}

	public static void convert(String[] args) throws IOException {
		if (args.length < 2) {
			System.out.println("Usage: <in> <out>\nConverts formats of mappings, requires correctly file extension to work.");
			System.exit(1);
		}
		MappingSet m = MappingSet.create();
		Utils.readMappings(m, AccessTransformSet.create(), Collections.singletonList(Paths.get(args[0])));
		Utils.writeMappings(Paths.get(args[1]), m);
		Utils.clean();
	}

	public static void inverse(String[] args) throws IOException {
		if (args.length < 2) {
			System.out.println("Usage: <in> <out>\nInverts mappings, optionally converts formats of mappings, requires correctly file extension to work.");
			System.exit(1);
		}
		MappingSet m = MappingSet.create();
		Utils.readMappings(m, AccessTransformSet.create(), Collections.singletonList(Paths.get(args[0])));
		m = Utils.invert(m);
		Utils.writeMappings(Paths.get(args[1]), m);
		Utils.clean();
	}

	public static void merge(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: <a> <b <out>\nMerges mappings, optionally converts formats of mappings, requires correctly file extension to work.");
			System.exit(1);
		}
		MappingSet a = MappingSet.create();
		Utils.readMappings(a, AccessTransformSet.create(), Collections.singletonList(Paths.get(args[0])));
		MappingSet b = MappingSet.create();
		Utils.readMappings(b, AccessTransformSet.create(), Collections.singletonList(Paths.get(args[1])));
		Utils.writeMappings(Paths.get(args[2]), Utils.merge(a, b));
		Utils.clean();
	}
}
