package ru.zaxar163.remappers.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.cadixdev.at.AccessTransformSet;
import org.cadixdev.lorenz.MappingSet;

import ru.zaxar163.remappers.ClassRemapper;
import ru.zaxar163.remappers.SourceRemapper;
import ru.zaxar163.remappers.Utils;

public class Remappers {
	public static List<Path> splitAsPaths(String before, boolean acceptDirs) throws IOException {
		if (before == null) return new ArrayList<>();
		List<Path> ret = before.contains(File.pathSeparator) ? Arrays.stream(before.split(File.pathSeparator)).map(Paths::get).collect(Collectors.toList())
				: new ArrayList<>(Collections.singletonList(Paths.get(before)));
		if (!acceptDirs) {
			List<Path> add = new ArrayList<Path>();
			List<Path> rem = new ArrayList<Path>();
			for (Path p : ret)
				if (Files.isDirectory(p)) {
					rem.add(p);
					Files.walk(p, Integer.MAX_VALUE).filter(e ->
						!Files.isDirectory(e) && Files.isReadable(e)).forEach(add::add);
				}
			ret.removeAll(rem);
			ret.addAll(add);
		}
		return ret;
	}
	
	public static void sourceRemap(Map<String, String> args) throws Throwable {
		if (!args.containsKey("in") || !args.containsKey("out") || !args.containsKey("mappings")) {
			System.out.println("Usage: --in=<src path> --out=<out path> --mappings=<mappings paths> --libs=[libs paths] --srcLibs=[srcLibs paths] --rewriteBridge=[true/false]");
			System.out.println("There specified 'paths' separate them via '" + File.pathSeparator + '\'');
			System.out.println("This tool remaps source code.");
			System.exit(1);
		}
		AccessTransformSet at = AccessTransformSet.create();
		MappingSet mh = MappingSet.create();
		Utils.readMappings(mh, at, splitAsPaths(args.get("mappings"), false));
		SourceRemapper.process(Paths.get(args.get("in")), Paths.get(args.get("out")), splitAsPaths(args.get("libs"), false), splitAsPaths(args.get("srcLibs"), true),
				mh, at, Boolean.parseBoolean(args.getOrDefault("rewriteBridge", "true")));
	}

	public static void classRemap(Map<String, String> args) throws Throwable {
		if (!args.containsKey("in") || !args.containsKey("out") || !args.containsKey("mappings")) {
			System.out.println("Usage: --in=<src path> --out=<out path> --mappings=<mappings paths> --libs=[libs paths]");
			System.out.println("There specified 'paths' separate them via '" + File.pathSeparator + '\'');
			System.out.println("This tool remap jars.");
			System.exit(1);
		}
		AccessTransformSet at = AccessTransformSet.create();
		MappingSet mh = MappingSet.create();
		Utils.readMappings(mh, at, splitAsPaths(args.get("mappings"), false));
		ClassRemapper.process(Paths.get(args.get("in")), Paths.get(args.get("out")), splitAsPaths(args.get("libs"), false), mh);
	}
}
